<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.Register');
    }

    public function thanks(Request $request){
        $pertama = $request ->first;
        $terakhir = $request ->last;
        return view('halaman.Welcome', compact('pertama', 'terakhir'));
    }
}
