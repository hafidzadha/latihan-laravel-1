<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<form action="/welcome" method="post">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>

        <label>First name:</label><br><br>
        <input type="text" name="first">
        <br>
        <br>
        <label>Last name:</label><br><br>
        <input type="text" name="last">
        <br>
        <br>
        <label>Gender</label><br><br>
        <input type="radio" name="gender" value="0">Male<br>
        <input type="radio" name="gender" value="1">Female<br>
        <input type="radio" name="gender" value="2">Other
        <br>
        <br>
        <label>Nationality</label><br><br>
        <select>
            <option value="indonesia">Indonesia</option>
            <option value="amerika">Amerika</option>
            <option value="inggris">Inggris</option>
        </select>
        <br>
        <br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="kemampuan_bahasa" value="0">Bahasa Indonesia <br>
        <input type="checkbox" name="kemampuan_bahasa" value="1">English <br>
        <input type="checkbox" name="kemampuan_bahasa" value="2">Other 
        <br>
        <br>
        <label>Bio:</label><br><br>
        <textarea cols="34" rows="11"></textarea>
        <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>